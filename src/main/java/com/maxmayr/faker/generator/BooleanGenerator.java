package com.maxmayr.faker.generator;

import java.util.Random;

public class BooleanGenerator implements Generator<Boolean>{

	@Override
	public Boolean getValue() {
		return new Random().nextBoolean();
	}

}
