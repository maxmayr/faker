package com.maxmayr.faker.generator;

import java.security.SecureRandom;

public class StringGenerator implements Generator<String> {
	private int min;
	private int max;
	private String characters;
	private static SecureRandom secureRandom = new SecureRandom();

	public StringGenerator(int min, int max, String characters) {
		this.min = min;
		this.max = max;
		this.characters = characters;
	}

	@Override
	public String getValue() {
		IntegerGenerator integer = new IntegerGenerator(min, max);
		return randomString(integer.getValue());
	}

	String randomString(int len) {
		StringBuilder stringBuilder = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			stringBuilder.append(characters.charAt(secureRandom.nextInt(characters.length())));
		return stringBuilder.toString();
	}

}
