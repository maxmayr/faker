package com.maxmayr.faker.generator;

public interface Generator<T> {
  T getValue();
}
