package com.maxmayr.faker.generator;

import java.util.concurrent.ThreadLocalRandom;

public class IntegerGenerator implements Generator<Integer>{
	private int min;
	private int max;
	public IntegerGenerator(int min, int max) {
		this.min = min;
		this.max = max;
	}
	@Override
	public Integer getValue() {
		return Integer.valueOf(ThreadLocalRandom.current().nextInt(min, max + 1));
	}

}
