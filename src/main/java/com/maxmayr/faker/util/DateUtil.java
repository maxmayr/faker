package com.maxmayr.faker.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	private DateUtil() {
		
	}
	public static int getDiffYears(Date first, Date last) {
	    Calendar startDate = getCalendar(first);
	    Calendar endDate = getCalendar(last);
	    int differnce = endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR);
	    if (startDate.get(Calendar.MONTH) > endDate.get(Calendar.MONTH) || 
	        (startDate.get(Calendar.MONTH) == endDate.get(Calendar.MONTH) && startDate.get(Calendar.DATE) > endDate.get(Calendar.DATE))) {
	        differnce--;
	    }
	    if(differnce < 0) {
	    	return differnce * -1;
	    }
	    return differnce;
	}

	public static Calendar getCalendar(Date date) {
	    Calendar calendar = Calendar.getInstance(Locale.US);
	    calendar.setTime(date);
	    return calendar;
	}
	
	public static Date createDate(int year,int month,int dayOfMonth) {
		LocalDate localDate = LocalDate.of(year, month, dayOfMonth);
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
}
