package com.maxmayr.faker.util;

import java.util.List;

public class ListUtil {

	private ListUtil() {

	}
	private static StringBuilder stringBuilder;

	public static String listToString(List<?> list) {
		stringBuilder = new StringBuilder();
		list.stream().map(ListUtil::getString).forEach(ListUtil::addToStringBuilder);
		return stringBuilder.toString();
	}

	private static String getString(Object element) {
		return element.toString();
	}
	
	private static void addToStringBuilder(String element) {
		stringBuilder.append(element);
	}

}
