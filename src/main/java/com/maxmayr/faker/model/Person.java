package com.maxmayr.faker.model;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.maxmayr.faker.fake.service.ProfessionService;

public class Person {
	private String firstName;
	private List<String> middleName;
	private String lastName;
	private Date birthday;
	private List<BankAccount> bankAccounts;
	private List<ProfessionService> profession;
	private List<Person> siblings;
	private List<Person> children;
	//This is only for Ronaldinho 
	private List<Person> spouse;
	private Locale nationality;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public List<String> getMiddleName() {
		return middleName;
	}
	public void setMiddleName(List<String> middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public List<BankAccount> getBankAccounts() {
		return bankAccounts;
	}
	public void setBankAccounts(List<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}
	public List<ProfessionService> getProfession() {
		return profession;
	}
	public void setProfession(List<ProfessionService> profession) {
		this.profession = profession;
	}
	public List<Person> getSiblings() {
		return siblings;
	}
	public void setSiblings(List<Person> siblings) {
		this.siblings = siblings;
	}
	public List<Person> getChildren() {
		return children;
	}
	public void setChildren(List<Person> children) {
		this.children = children;
	}
	public List<Person> getSpouse() {
		return spouse;
	}
	public void setSpouse(List<Person> spouse) {
		this.spouse = spouse;
	}
	public Locale getNationality() {
		return nationality;
	}
	public void setNationality(Locale nationality) {
		this.nationality = nationality;
	}
	
}
