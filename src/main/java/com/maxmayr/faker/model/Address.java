package com.maxmayr.faker.model;

public class Address {
	private String street;
	private String houseNummer;
	private String zip;
	private String country;
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNummer() {
		return houseNummer;
	}
	public void setHouseNummer(String houseNummer) {
		this.houseNummer = houseNummer;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
