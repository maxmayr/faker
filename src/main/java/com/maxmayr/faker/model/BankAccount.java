package com.maxmayr.faker.model;

import java.util.Date;
import java.util.List;

import org.iban4j.Iban;

public class BankAccount {
	private Iban iban;
	private List<Person> accountHolders;
	private Date createDate;

	public Iban getIban() {
		return iban;
	}

	public void setIban(Iban iban) {
		this.iban = iban;
	}

	public List<Person> getAccountHolders() {
		return accountHolders;
	}

	public void setAccountHolders(List<Person> accountHolders) {
		this.accountHolders = accountHolders;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
