package com.maxmayr.faker.locale;

public enum FakerLocale {
	EN("en");
	private String locale;

	FakerLocale(String locale) {
        this.locale = locale;
    }

    public String locale() {
        return locale;
    }
}
