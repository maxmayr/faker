package com.maxmayr.faker.reader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.yaml.snakeyaml.Yaml;

import com.maxmayr.faker.locale.FakerLocale;

public class YamlReader {
	private static final String ENCODING = "UTF-8";
	private FakerLocale fakerLocale;
	private String valueName;
	private Random randomGenerator;

	public YamlReader(FakerLocale fakerLocale, String valueName) {
		this.fakerLocale = fakerLocale;
		this.valueName = valueName;
		randomGenerator = new Random();
	}

	public YamlReader(FakerLocale fakerLocale) {
		this.fakerLocale = fakerLocale;
		randomGenerator = new Random();
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	public List<String> getStrings() {
		LinkedHashMap<?, ?> result = (LinkedHashMap<?, ?>) getMap();
		return (List<String>) result.get(this.valueName);
	}

	public String getRandomString() {
		LinkedHashMap<?, ?> result = (LinkedHashMap<?, ?>) getMap();
		ArrayList<String> values = (ArrayList<String>) result.get(this.valueName);
		if(values == null) {
			return null;
		}
		Optional<?> anyValue = anyValue(values);
		if(anyValue.isPresent()) {
			return (String) anyValue.get();
		}else {
			return null;
		}
	}

	public Map<String, Object> getRandomProperty() {
		LinkedHashMap<?, ?> result = (LinkedHashMap<?, ?>) getMap();
		ArrayList<LinkedHashMap<?, ?>> values = (ArrayList<LinkedHashMap<?, ?>>) result.get(this.valueName);
		if(values == null) {
			return null;
		}
		Optional<?> anyValue = anyValue(values);
		if(anyValue.isPresent()) {
			return (Map<String, Object>) anyValue.get();
		}else {
			return null;
		}
		
	}

	public Integer getRandomInteger() {
		LinkedHashMap<?, ?> result = (LinkedHashMap<?, ?>) getMap();
		ArrayList<Integer> values =  (ArrayList<Integer>) result.get(this.valueName);
		if(values == null) {
			return null;
		}
		Optional<?> anyValue = anyValue(values);
		if(anyValue.isPresent()) {
			return (Integer) anyValue.get();
		}else {
			return null;
		}
	}

	public String getPropertyAsString(String value, String propetyName) {
		Yaml yaml = new Yaml();
		LinkedHashMap<?, ?> result = yaml.load(value);
		return (String) result.get(propetyName);
	}

	private Map<?, ?> getMap() {
		InputStream inputStream = findStream(fakerLocale.locale());
		String yamlString = null;
		try {
			yamlString = inputStreamToString(inputStream);
		} catch (IOException e) {
			Logger log = Logger.getAnonymousLogger();
			log.log(Level.ALL, "Error while parsing yaml", e);
		}
		Yaml yaml = new Yaml();
		return yaml.load(yamlString);
	}

	private String inputStreamToString(InputStream inputStream) throws IOException {
		final char[] buffer = new char[1024];
		final StringBuilder stringBuilder = new StringBuilder();
		Reader reader = new InputStreamReader(inputStream, ENCODING);
		for (;;) {
			int actual = reader.read(buffer, 0, buffer.length);
			if (actual < 0) {
				break;
			}
			stringBuilder.append(buffer, 0, actual);
		}
		return stringBuilder.toString();
	}

	private InputStream findStream(String filename) {
		String filenameWithExtension = File.pathSeparator + filename + ".yml";
		InputStream streamOnClass = getClass().getResourceAsStream(filenameWithExtension);
		if (streamOnClass != null) {
			return streamOnClass;
		}
		return getClass().getClassLoader().getResourceAsStream(filenameWithExtension);
	}

	private Optional<?> anyValue(ArrayList<?> list) {
		int index = randomGenerator.nextInt(list.size());
		return Optional.of(list.get(index));
	}

	public FakerLocale getLocale() {
		return this.fakerLocale;
	}

}
