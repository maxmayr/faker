package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.reader.YamlReader;

public class FirstNameService implements FakerService<String>{
	private YamlReader yamlReader;

	public FirstNameService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "first_name");
	}
	
	@Override
	public String value() {		
		return this.value(new RandomBooleanService().value());
	}

	public String value(boolean requireMale) {
		if (!requireMale)
			this.yamlReader.setValueName("first_name_female");
		return yamlReader.getRandomString();
	}
}
