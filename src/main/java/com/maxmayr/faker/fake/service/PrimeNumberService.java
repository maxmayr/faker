package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.reader.YamlReader;

public class PrimeNumberService implements FakerService<Integer>{
	private YamlReader yamlReader;
	public PrimeNumberService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "primenumber");
	}
	@Override
	public Integer value() {
		return yamlReader.getRandomInteger();
	}

}
