package com.maxmayr.faker.fake.service;

import java.util.ArrayList;
import java.util.Date;

import org.iban4j.CountryCode;
import org.iban4j.Iban;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.model.BankAccount;
import com.maxmayr.faker.model.Person;
import com.maxmayr.faker.reader.YamlReader;

public class BankAccountService implements FakerService<BankAccount>{

	private YamlReader yamlReader;
	public BankAccountService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "bank_account");
	}
	
	@Override
	public BankAccount value() {
		BankAccount bankAccount = new BankAccount();
		bankAccount.setIban(iban());
		
		ArrayList<Person> persons = new ArrayList<>();
		Person person = new PersonService(this.yamlReader.getLocale()).value();
		persons.add(person);
		bankAccount.setAccountHolders(persons);
		Date date = new BirthdayService(this.yamlReader.getLocale()).value(0, 5);
		bankAccount.setCreateDate(date);
		return bankAccount;
	}
	
	public Iban iban() {
		return Iban.random(CountryCode.GB);
	}
	
}
