package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.reader.YamlReader;

public class AnimalService implements FakerService<String>{

	private YamlReader yamlReader;
	public AnimalService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "animal");
	}
	@Override
	public String value() {
		return yamlReader.getRandomString();
	}

}
