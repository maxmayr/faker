package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.generator.BooleanGenerator;

public class RandomBooleanService implements FakerService<Boolean>{
	@Override
	public Boolean value() {
		return new BooleanGenerator().getValue();
	}
}
