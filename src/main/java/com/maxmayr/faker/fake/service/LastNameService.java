package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.reader.YamlReader;

public class LastNameService implements FakerService<String>{

	private YamlReader yamlReader;
	public LastNameService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "last_name");
	}
	@Override
	public String value() {
		return yamlReader.getRandomString();
	}
}
