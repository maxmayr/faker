package com.maxmayr.faker.fake.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.maxmayr.faker.generator.IntegerGenerator;
import com.maxmayr.faker.locale.FakerLocale;

public class BirthdayService implements FakerService<Date>{


	public BirthdayService(FakerLocale fakerLocale) {

	}
	public Date value(int minAge,int maxAge) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		int actualYear = Calendar.getInstance().get(Calendar.YEAR);
        int year = new IntegerGenerator(actualYear-maxAge,actualYear-minAge).getValue();
        gregorianCalendar.set(Calendar.YEAR, year);
        int dayOfYear = new IntegerGenerator(1,gregorianCalendar.getActualMaximum(Calendar.DAY_OF_YEAR)).getValue();
        gregorianCalendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
		return gregorianCalendar.getTime();
    }
	@Override
	public Date value() {
		return value(18,70);
	}
}
