package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.reader.YamlReader;

public class ProfessionService implements FakerService<String>{
	private YamlReader yamlReader;
	public ProfessionService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "profession");
	}
	@Override
	public String value() {
		return yamlReader.getRandomString();
	}
}
