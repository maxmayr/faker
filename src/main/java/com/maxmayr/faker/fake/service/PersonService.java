package com.maxmayr.faker.fake.service;

import java.util.ArrayList;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.model.BankAccount;
import com.maxmayr.faker.model.Person;
import com.maxmayr.faker.reader.YamlReader;

public class PersonService implements FakerService<Person>{


	private YamlReader yamlReader;
	public PersonService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale, "person");
	}
	
	@Override
	public Person value() {
		return value(18,99,false);
	}
	
	public Person value(Integer minAge,Integer maxAge,Boolean hasBankAccount) {
		Person person = new Person();
		FirstNameService firstNameService = new FirstNameService(this.yamlReader.getLocale());
		String firstName = firstNameService.value();
		person.setFirstName(firstName);
		String middleName = firstNameService.value();
		while(middleName.equals(firstName)) {
			middleName = firstNameService.value();
		}
		ArrayList<String> middleNames = new ArrayList<>();
		middleNames.add(middleName);
		person.setMiddleName(middleNames);
		
		if(hasBankAccount) {
			ArrayList<BankAccount> bankAccounts = new ArrayList<>();
			bankAccounts.add(getBankAccount());
			person.setBankAccounts(bankAccounts);
		}
		person.setBirthday(new BirthdayService(this.yamlReader.getLocale()).value(minAge,maxAge));
		return person;
	}
	
	private BankAccount getBankAccount() {
		return new BankAccountService(yamlReader.getLocale()).value();
	}
}
