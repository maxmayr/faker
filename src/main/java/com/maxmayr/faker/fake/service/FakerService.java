package com.maxmayr.faker.fake.service;

public interface FakerService<T> {
	T value();
}
