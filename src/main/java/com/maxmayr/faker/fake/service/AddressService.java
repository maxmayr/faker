package com.maxmayr.faker.fake.service;

import java.util.Map;

import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.model.Address;
import com.maxmayr.faker.reader.YamlReader;

public class AddressService implements FakerService<com.maxmayr.faker.model.Address> {
	private static final String DEFAULT_HOUSENUMMER = "0";
	private static final String ZIP = "zip";
	private static final String HAS_ONLY_NUMMER_REGEX = "[0-9]+";
	private static final String HOUSENUMMER = "housenummer";
	private static final String STREET = "street";
	private static final String ADDRESS = "address";
	private static final Object CITY = "city";
	private static final Object COUNTRY = "country";
	private YamlReader yamlReader;

	public AddressService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale);
		yamlReader.setValueName(ADDRESS);
	}

	@Override
	public Address value() {
		com.maxmayr.faker.model.Address address = new com.maxmayr.faker.model.Address();
		Map<?, ?> addressMap = yamlReader.getRandomProperty();
		address.setCountry((String) addressMap.get(COUNTRY));
		address.setHouseNummer((String) addressMap.get(HOUSENUMMER));
		address.setStreet((String) addressMap.get(STREET));
		address.setZip((String) addressMap.get(ZIP));
		return address;
	}

	public String street() {
		Map<?, ?> address = yamlReader.getRandomProperty();
		return (String) address.get(STREET);
	}

	public String houseNummer() {
		Map<?, ?> address = yamlReader.getRandomProperty();
		return (String) address.get(HOUSENUMMER);
	}

	public String houseNummer(boolean mustHaveLetter) {
		String result = DEFAULT_HOUSENUMMER;
		while (match(result, mustHaveLetter)) {
			Map<?, ?> address = yamlReader.getRandomProperty();
			result = (String) address.get(HOUSENUMMER);
		}
		return result;
	}

	public String city() {
		Map<?, ?> address = yamlReader.getRandomProperty();
		return (String) address.get(CITY);
	}

	public String country() {
		Map<?, ?> address = yamlReader.getRandomProperty();
		return (String) address.get(COUNTRY);
	}

	private boolean match(String result, Boolean mustHaveLetter) {
		boolean match = checkIfItHasALetter(result);
		if (mustHaveLetter) {
			return match;
		} else {
			return !match;
		}
	}

	private boolean checkIfItHasALetter(String result) {
		return result.matches(HAS_ONLY_NUMMER_REGEX);
	}

	public String zip() {
		Map<?, ?> address = yamlReader.getRandomProperty();
		return (String) address.get(ZIP);
	}

}
