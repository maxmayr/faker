package com.maxmayr.faker.fake.service;

import com.maxmayr.faker.generator.StringGenerator;
import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.reader.YamlReader;
import com.maxmayr.faker.util.ListUtil;

public class RandomStringService {
	private YamlReader yamlReader;

	public RandomStringService(FakerLocale fakerLocale) {
		this.yamlReader = new YamlReader(fakerLocale);
		this.yamlReader.setValueName("characters");
	}

	public String getValue(int min, int max) {
		String characters = ListUtil.listToString(yamlReader.getStrings());
		return new StringGenerator(min, max, characters).getValue();
	}

}
