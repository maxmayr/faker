package com.maxmayr.faker;

import com.maxmayr.faker.fake.service.AddressService;
import com.maxmayr.faker.fake.service.AnimalService;
import com.maxmayr.faker.fake.service.BankAccountService;
import com.maxmayr.faker.fake.service.BirthdayService;
import com.maxmayr.faker.fake.service.FirstNameService;
import com.maxmayr.faker.fake.service.LastNameService;
import com.maxmayr.faker.fake.service.PersonService;
import com.maxmayr.faker.fake.service.PlantService;
import com.maxmayr.faker.fake.service.PrimeNumberService;
import com.maxmayr.faker.fake.service.ProfessionService;
import com.maxmayr.faker.fake.service.RandomBooleanService;
import com.maxmayr.faker.fake.service.RandomStringService;
import com.maxmayr.faker.locale.FakerLocale;
/**
 * @author  Max Mayr
 * @version 0.0.1
 */
public class Faker {
	private FirstNameService firstName;
	private LastNameService lastName;
	private PlantService plant;
	private AnimalService animal;
	private AddressService address;
	private RandomStringService randomString;
	private RandomBooleanService randomBoolean;
	private ProfessionService profession;
	private BirthdayService birthday;
	private PersonService person;
	private BankAccountService bankAccount;

	public BankAccountService getBankAccount() {
		return bankAccount;
	}
	public ProfessionService getProfession() {
		return profession;
	}
	private PrimeNumberService primeNumber;

	public Faker() {
		this(FakerLocale.EN);
	}
	/*
	 * @param fakerLocale 
	 */
	public Faker(FakerLocale fakerLocale) {
		firstName = new FirstNameService(fakerLocale);
		lastName = new LastNameService(fakerLocale);
		plant = new PlantService(fakerLocale);
		animal = new AnimalService(fakerLocale);
		address = new AddressService(fakerLocale);
		randomString = new RandomStringService(fakerLocale);
		primeNumber = new PrimeNumberService(fakerLocale);
		randomBoolean = new RandomBooleanService();
		profession  = new ProfessionService(fakerLocale);
		birthday = new BirthdayService(fakerLocale);
		person = new PersonService(fakerLocale);
		bankAccount = new BankAccountService(fakerLocale);
	}

	public PersonService getPerson() {
		return person;
	}
	public BirthdayService getBirthday() {
		return birthday;
	}
	public String getFirstName() {	
		return firstName.value();
	}
	public String getFirstName(boolean requireMale) {
		return firstName.value(requireMale);
	}

	public String getLastName() {
		return lastName.value();
	}
	
	public String getPlant() {
		return plant.value();
	}
	
	public String getAnimal() {
		return animal.value();
	}
	public AddressService getAddress() {
		return address;
	}
	public String getRandomString(int min, int max) {
		return randomString.getValue(min, max);
	}
	public PrimeNumberService getPrimeNumber() {
		return primeNumber;
	}
	public RandomBooleanService getRandomBoolean() {
		return randomBoolean;
	}
	
}
