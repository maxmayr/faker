package com.maxmayr.faker.lokale;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.maxmayr.faker.locale.FakerLocale;

public class FakerLocalTests {
	@Test
	public void test_lokale() {
		FakerLocale.valueOf("EN");
		String result = FakerLocale.EN.locale();
		assertTrue("Is en?", result.equals("en"));
	}
}
