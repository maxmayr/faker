package com.maxmayr.faker.generator;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringGeneratorTests {
	@Test
	public void test_random_string() {
		StringGenerator sg = new StringGenerator(0, 1, "ABC");
		String s = sg.randomString(8);
		assertTrue("Is not null?",s!=null);
		assertTrue("Is not empty?",!s.isEmpty());
	}
}
