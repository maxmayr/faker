package com.maxmayr.faker.util;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class DateUtilTests {
	@Test
	public void check_if_difference_is_right() {
		Date one = DateUtil.createDate(2018, 01, 01);
		Date two = DateUtil.createDate(2015, 01, 01);
		Integer diff = DateUtil.getDiffYears(one, two);
		assertTrue("Is diff 3?",diff ==  3);
	}
}
