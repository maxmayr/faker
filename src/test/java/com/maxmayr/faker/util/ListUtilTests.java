package com.maxmayr.faker.util;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class ListUtilTests {
	
	@Test
	public void test_list(){
		ArrayList<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		String result = ListUtil.listToString(list);
		assertTrue("List is ab?",result.equals("ab"));
	}
}
