package com.maxmayr.faker;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import com.maxmayr.faker.fake.service.PersonService;
import com.maxmayr.faker.fake.service.RandomBooleanService;
import com.maxmayr.faker.locale.FakerLocale;
import com.maxmayr.faker.model.Address;
import com.maxmayr.faker.model.BankAccount;
import com.maxmayr.faker.model.Person;
import com.maxmayr.faker.util.DateUtil;

public class FakerIntegrationTests {
	@Test
	public void integrationTest_first_name() {
		Faker faker = new Faker();
		String result = faker.getFirstName();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_first_name_male() {
		Faker faker = new Faker();
		String result = faker.getFirstName(true);
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_first_name_female() {
		Faker faker = new Faker();
		String result = faker.getFirstName(false);
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_last_name() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getLastName();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_plant() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getPlant();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_animal() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAnimal();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_housenummer() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().houseNummer();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_housenummer_must_have_letter() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().houseNummer(true);
		boolean hasLetter = !result.matches("[0-9]+");
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
		assertTrue("has letter?", hasLetter);
	}

	@Test
	public void integrationTest_housenummer_must_not_have_letter() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().houseNummer(false);
		boolean hasLetter = result.matches("[0-9]+");
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
		assertTrue("has letter?", hasLetter);
	}

	@Test
	public void integrationTest_street() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().street();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_zip() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().zip();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_city() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().city();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_country() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getAddress().country();
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_address_object() {
		Faker faker = new Faker(FakerLocale.EN);
		Address result = faker.getAddress().value();
		assertTrue("Is not null?", result != null);
		assertTrue("Is country not null?", result.getCountry() != null);
		assertTrue("Is country not empty?", !result.getCountry().isEmpty());
		assertTrue("Is house nummer not null?", result.getHouseNummer() != null);
		assertTrue("Is house nummer not empty?", !result.getHouseNummer().isEmpty());
		assertTrue("Is street not null?", result.getStreet() != null);
		assertTrue("Is street not empty?", !result.getStreet().isEmpty());
		assertTrue("Is zip not null?", result.getZip() != null);
		assertTrue("Is zip not empty?", !result.getZip().isEmpty());
	}

	@Test
	public void integrationTest_random_string() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getRandomString(1, 5);
		assertTrue("Is not empty?", !result.isEmpty());
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_prime_number() {
		Faker faker = new Faker(FakerLocale.EN);
		Integer result = faker.getPrimeNumber().value();
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_boolean() {
		Faker faker = new Faker(FakerLocale.EN);
		RandomBooleanService result = faker.getRandomBoolean();
		assertTrue("Is not null?", result.value() != !result.value());
	}

	@Test
	public void integrationTest_profession() {
		Faker faker = new Faker(FakerLocale.EN);
		String result = faker.getProfession().value();
		assertTrue("Is not null?", result != null);
		assertTrue("Is not empty?", !result.isEmpty());
	}

	@Test
	public void integrationTest_birthday() {
		Faker faker = new Faker(FakerLocale.EN);
		Date result = faker.getBirthday().value(1, 100);
		assertTrue("Is not null?", result != null);
	}
	
	@Test
	public void integrationTest_birthday_without_params() {
		Faker faker = new Faker(FakerLocale.EN);
		Date result = faker.getBirthday().value();
		assertTrue("Is not null?", result != null);
	}

	@Test
	public void integrationTest_bankaccount() {
		Faker faker = new Faker(FakerLocale.EN);
		BankAccount result = faker.getBankAccount().value();
		assertTrue("Is not null?", result != null);
		assertTrue("Has one account holder?", result.getAccountHolders().size() > 0);

		assertTrue("Is create date not null?", result.getCreateDate() != null);
		assertTrue("Is iban not null?", result.getIban() != null);
	}

	@Test
	public void integrationTest_person() {
		Faker faker = new Faker(FakerLocale.EN);
		Person result = faker.getPerson().value(18, 44, true);

		assertTrue("Is not null?", result != null);

		assertTrue("Is bank account size bigger than 0?", result.getBankAccounts().size() > 0);
		assertTrue("Is iban on first bank account set?", result.getBankAccounts().get(0).getIban() != null);
		assertTrue("Is create date on first bank account set?", result.getBankAccounts().get(0).getCreateDate() != null);
		
		assertTrue("Is firstname set?", result.getFirstName() != null);
		assertTrue("Has 1 middle name?", result.getMiddleName().size() == 1);
		
		assertTrue("Are firstname and middle name not the same?", !result.getFirstName().equals(result.getMiddleName().get(0)));
		
		int diff =  DateUtil.getDiffYears(result.getBirthday(), new Date());
		assertTrue("Is iban on first bank account set?",diff >= 18  && diff <= 44);

	}

}
