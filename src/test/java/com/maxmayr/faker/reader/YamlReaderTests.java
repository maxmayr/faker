package com.maxmayr.faker.reader;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Map;

import org.junit.Test;

import com.maxmayr.faker.locale.FakerLocale;

public class YamlReaderTests {
	@Test
	public void test_get_property() {
		String value = "{\"housenummer\":\"1\", \"street\":\"Lister Ho Lomas St\",\"city\": \"London\", \"zip\":\"E1 5BG\"}";
		YamlReader yamlreader = new YamlReader(FakerLocale.EN);
		String property = yamlreader.getPropertyAsString(value, "housenummer");
		assertTrue("Is housnumber not null?", property != null);
		assertTrue("Is housnumber is 1?", property.equals("1"));
	}

	@Test
	public void test_findStream() {
		YamlReader yamlreader = new YamlReader(FakerLocale.EN);
		InputStream streamOnClass = getClass().getResourceAsStream("");
		Map<?, ?> isNull = yamlreader.getRandomProperty();
		Integer isNullInteger = yamlreader.getRandomInteger();
		String isNullString = yamlreader.getRandomString();
		assertTrue("Is null?", isNull == null);
		assertTrue("Is null?", isNullInteger == null);
		assertTrue("Is null?", isNullString == null);
	}
}
